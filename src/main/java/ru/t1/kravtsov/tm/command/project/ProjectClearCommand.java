package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.model.Project;

import java.util.List;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    public static final String NAME = "project-clear";

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        @NotNull final List<Project> projects = getProjectService().findAll();
        getProjectTaskService().removeProjects(getUserId(), projects);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
