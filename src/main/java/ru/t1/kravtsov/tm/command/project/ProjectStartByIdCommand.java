package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Start project by id.";

    @NotNull
    public static final String NAME = "project-start-by-id";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(getUserId(), id, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
