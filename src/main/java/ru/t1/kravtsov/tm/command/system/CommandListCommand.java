package ru.t1.kravtsov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.command.AbstractCommand;

public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-c";

    @NotNull
    public static final String DESCRIPTION = "Display application commands.";

    @NotNull
    public static final String NAME = "commands";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        for (AbstractCommand command : getCommandService().getTerminalCommands()) {
            @NotNull final String name = command.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
