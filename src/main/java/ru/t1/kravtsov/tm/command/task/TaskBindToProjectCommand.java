package ru.t1.kravtsov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectID = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskID = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(getUserId(), projectID, taskID);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
